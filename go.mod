module findmydeviceserver

go 1.17

require (
	github.com/google/flatbuffers v23.5.26+incompatible
	github.com/objectbox/objectbox-go v1.7.0
	golang.org/x/crypto v0.23.0
	gopkg.in/yaml.v3 v3.0.1
	gorm.io/driver/sqlite v1.5.5
	gorm.io/gorm v1.25.10
)

require (
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/jinzhu/now v1.1.5 // indirect
	github.com/mattn/go-sqlite3 v1.14.17 // indirect
	github.com/objectbox/objectbox-generator v0.13.0 // indirect
)
